### Compilation in Docker
```
docker run -it ubuntu:rolling /bin/bash

apt update

apt install -y git

git clone https://sourceware.org/git/valgrind.git
cd valgrind

sed -Ei 's/^# deb-src /deb-src /' /etc/apt/sources.list

export DEBIAN_FRONTEND=noninteractive DEBCONF_NONINTERACTIVE_SEEN=true

apt update

#Z#
apt build-dep -y valgrind

./autogen.sh
./configure

make -j8

```

